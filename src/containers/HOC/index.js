import React from "react";

export const HOC = Component =>
	class extends React.Component {
		state = {
			error: false,
			config: {
				card: {
					header: {
						title: "",
						color: "#333",
						backgroundColor: "#ddd"
					},
					classNames: "col-lg-8"
				}
			}
		};

		componentWillMount() {
			!!this.props.config &&
				this.setState({
					config: this.props.config
				});
		}

		componentDidCatch(error, info) {
			this.setState({ error: true });
			this.logError(error, info);
		}

		get value() {
			return this.config;
		}

		logError = (error, info) => console.log(error, info);

		render() {
			const { error, config } = this.state;

			return (
				<div className="row">
					<div className={config.card.classNames}>
						<div className="card">
							<div
								style={{
									background: config.card.header.backgroundColor,
									color: config.card.header.color
								}}
								className="card-header"
							>
								{config.card.header.title}
							</div>
							<div className="card-body">
								{!error ? (
									<Component {...this.props} />
								) : (
									<p>Something went wrong ..!</p>
								)}
							</div>
						</div>
					</div>
				</div>
			);
		}
	};
