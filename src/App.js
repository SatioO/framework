import React, { Component } from "react";
import { Chart } from "./containers/Chart";
import { HOC } from "./containers/HOC";
const ChartComponent = HOC(Chart);

class App extends Component {
	config = {
		card: {
			header: {
				title: "Chart Component",
				color: "#FFF",
				backgroundColor: "#333"
			},
			classNames: "col-md-8"
		}
	};

	render() {
		return (
			<div className="offset-md-3">
				<ChartComponent config={this.config} name="chart will render here" />
			</div>
		);
	}
}

export default App;
